﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Imaging;
using System.IO;
using System.Diagnostics;

namespace DIP
{
    public partial class Form1 : Form
    {
        public bool havePic=false;
        public string path;
        public Color[,] img;
        public int[] size=new int[2];

        public Form1()
        {
            InitializeComponent();
        }



        public Color[,] GetPix(string path)
        {
            Bitmap oldbitmap = new Bitmap(path);
            this.size[0] = oldbitmap.Width;
            this.size[1] = oldbitmap.Height;
            img=new Color[this.size[0],size[1]];
            Color[,] pixels = new Color[size[0],size[1]];
            for (int x = 0; x < this.size[0]; x++)
            {
                for (int y = 0; y < this.size[1]; y++)
                {
                    pixels[x,y] = oldbitmap.GetPixel(x,y);
                }
            }
            return pixels;
        }


        /// <summary>
        /// 灰度
        /// </summary>
        public void Grey() {
            int width=this.size[0];
            int height=this.size[1];
            int x,y;
            for(x=0;x<width;x++){
                for (y = 0; y<height;y++ ) {
                    byte avg=(byte)(((int)img[x,y].R + (int)img[x,y].G + (int)img[x,y].B) / 3);
                    img[x,y] = Color.FromArgb(avg, avg, avg); 
                }
            };
        }

        /// <summary>
        /// 二值化
        /// </summary>
        public void Bi() {
            int width = this.size[0];
            int height = this.size[1];
            int x, y;
            for (x = 0; x < width; x++)
            {
                for (y = 0; y < height; y++)
                {
                    byte avg = (byte)(((int)img[x, y].R + (int)img[x, y].G + (int)img[x, y].B) / 3);
                    if (avg <= 127) {
                        avg = 0;
                    }else if(avg>127){
                        avg = 255;
                    }
                    img[x, y] = Color.FromArgb(avg, avg, avg);
                }
            };
        }

        /// <summary>
        /// 高斯模糊
        /// </summary>
        public void Gaosi() {           
            int width = this.size[0];
            int height = this.size[1];
            int x, y;
            int rp1,gp1,bp1,    
                 rp2,gp2,bp2,    
                 rp3,gp3,bp3,    
                 rp4,gp4,bp4,    
                 rp5,gp5,bp5,    
                 rp6,gp6,bp6,    
                 rp7,gp7,bp7,    
                 rp8,gp8,bp8;

            if (width <= 1 || height <= 1) { return; }
            for (x = 1; x < width-1; x++)
            {
                for (y = 1; y < height-1; y++)
                {
                    ///1
                    rp1=(int)img[x-1, y].R;
                    gp1=(int)img[x-1, y].G;
                    bp1=(int)img[x-1, y].B;
                    ///2
                    rp2=(int)img[x+1, y].R;
                    gp2=(int)img[x+1, y].G;
                    bp2=(int)img[x+1, y].B;
                    ///3
                    rp3=(int)img[x-1, y-1].R;
                    gp3=(int)img[x-1, y-1].G;
                    bp3=(int)img[x-1, y-1].B;
                    ///4
                    rp4=(int)img[x+1, y-1].R;
                    gp4=(int)img[x+1, y-1].G;
                    bp4=(int)img[x+1, y-1].B;
                    ///5
                    rp5=(int)img[x, y-1].R;
                    gp5=(int)img[x, y-1].G;
                    bp5=(int)img[x, y-1].B;
                    ///6
                    rp6=(int)img[x-1, y+1].R;
                    gp6=(int)img[x-1, y+1].G;
                    bp6=(int)img[x-1, y+1].B;
                    ///7
                    rp7=(int)img[x+1, y+1].R;
                    gp7=(int)img[x+1, y+1].G;
                    bp7=(int)img[x+1, y+1].B;
                    ///8
                    rp8=(int)img[x, y+1].R;
                    gp8=(int)img[x, y+1].G;
                    bp8=(int)img[x, y+1].B;

                    int r = (byte)((rp1 + rp2 + rp3 + rp4 + rp5 + rp6 + rp7 + rp8) / 8);
                    int g = (byte)((gp1 + gp2 + gp3 + gp4 + gp5 + gp6 + gp7 + gp8) / 8);
                    int b = (byte)((bp1 + bp2 + bp3 + bp4 + bp5 + bp6 + bp7 + bp8) / 8);
                    
                    img[x, y] = Color.FromArgb(r, g, b);
                }
            }
        }

        /// <summary>
        /// 选择图片
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pictureBox1_Click(object sender, EventArgs e)
        {
            FileDialog of=new OpenFileDialog();
            of.Filter = "图片文件(*.gif;*.jpg;*.jpeg;*.bmp;*.wmf;*.png)|*.gif;*.jpg;*.jpeg;*.bmp;*.wmf;*.png";
            if (of.ShowDialog() == DialogResult.OK) {
                this.path=of.FileName;
                pictureBox1.ImageLocation = this.path;
                img = GetPix(this.path);
                havePic = true;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!havePic) { return; }
            this.Grey();
            int w=size[0];
            int h=size[1];
            int x,y;
            Bitmap bitmap = new Bitmap(path);

            for(x=0;x<w;x++){
                for (y = 0; y < h;y++ ) {
                    bitmap.SetPixel(x,y,img[x,y]);
                }
            }

            pictureBox1.Image = bitmap;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (!havePic) { return; }
            Bi();
            int w = size[0];
            int h = size[1];
            int x, y;
            Bitmap bitmap = new Bitmap(path);

            for (x = 0; x < w; x++)
            {
                for (y = 0; y < h; y++)
                {
                    bitmap.SetPixel(x, y, img[x, y]);
                }
            }

            pictureBox1.Image = bitmap;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            long start = Utils.UnixTimestamp();
            if (!havePic) { return; }
            for (int i = 0; i < 30;i++ )
            {
                Gaosi();
            }
            long end = Utils.UnixTimestamp();

            Trace.TraceInformation("高斯模糊耗时: {0}s", end - start);

            int w = size[0];
            int h = size[1];
            int x, y;
            Bitmap bitmap = new Bitmap(path);

            for (x = 0; x < w; x++)
            {
                for (y = 0; y < h; y++)
                {
                    bitmap.SetPixel(x, y, img[x, y]);
                }
            }

            pictureBox1.Image = bitmap;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (!havePic) { return; }
            Bitmap bitmap = new Bitmap(path);
            int w = size[0];
            int h = size[1];
            int x, y;
            for (x = 0; x < w; x++)
            {
                for (y = 0; y < h; y++)
                {
                    bitmap.SetPixel(x, y, img[x, y]);
                }
            }
            FileDialog sd = new SaveFileDialog();
            sd.Filter = "位图(*.bmp)|*.bmp|PNG(*.png)|*.png|JPEG(*.jpg)|*.jpg|GIF(*.gif)|*.gif";
            
            if(sd.ShowDialog() == DialogResult.OK)
            {
                string ext = Path.GetExtension(sd.FileName);
                if (".bmp" == ext)
                {
                    bitmap.Save(sd.FileName, ImageFormat.Bmp);
                }
                else if(".png" == ext)
                {
                    bitmap.Save(sd.FileName, ImageFormat.Png);
                }else if(".gif" == ext)
                {
                    bitmap.Save(sd.FileName, ImageFormat.Gif);
                }else if (".jpg" == ext)
                {
                    bitmap.Save(sd.FileName, ImageFormat.Jpeg);
                }
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.pictureBox1_Click(sender, e);
        }
    }
}
