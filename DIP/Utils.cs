﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIP
{
    class Utils
    {
        public static long UnixTimestamp()
        {
            System.DateTime startTime = TimeZone.CurrentTimeZone.ToLocalTime(new System.DateTime(1970, 1, 1));
            return (long)(DateTime.Now - startTime).TotalSeconds;
        }
    }
}
